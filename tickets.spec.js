describe("Tickets", () => {
    beforeEach(() => cy.visit("https://ticket-box.s3.eu-central-1.amazonaws.com/index.html"));

    it("Preenche todos os campos de entrada de texto", () => {
        const firstName = "Leonardo";
        const lastName = "Siqueira";

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type('leonardogabriel1203@gmail.com');
        cy.get("#requests").type('Não');
        cy.get("#signature").type(`${firstName} ${lastName}`);
    });

    it("Selecione dois ingressos", () => {
        cy.get("#ticket-quantity").select("2");
    });

    it("Selecione o tipo de ingresso 'vip'", () => {
        cy.get("#vip").check();
    });

    it("Selecione a caixa de seleção 'mídia social'", () => {
        cy.get("#social-media").check();
    });

    it("Selecione 'friend' e 'publication', em seguida desmarque 'friend'", () => {
        cy.get("#friend").check();
        cy.get("#publication").check();
        cy.get("#friend").uncheck();
    });

    it("Possui cabeçalho de 'TICKETBOX'", () => {
        cy.get("header h1").should("contain", "TICKETBOX");
    });

    it("Alertas sobre e-mail inválido", () => {
        cy.get("#email")
        .as("email")
        .type("exemplo.invalido-gmail.com");

        cy.get("#email.invalid").should("exist");
        
        cy.get("@email")
        .clear()
        .type("exemplo.valido@gmail.com");

        cy.get("#email.invalid").should("not.exist");

     });

     it("Preenche e renicia o formulário", () => {
        const firstName = "Leonardo";
        const lastName = "Siqueira";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type('leonardogabriel1203@gmail.com');
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#social-media").check();
        cy.get("#friend").check();
        cy.get("#requests").type('Lager beer');
        cy.get(".agreement p").should("contain", `I, ${fullName}, wish to buy 2 VIP tickets.`);

        

        cy.get("#agree").click();
        cy.get("#signature").type(fullName);

        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");

        cy.get(".reset").click();

        cy.get("@submitButton").should("be.disabled");
        
    });

    it("Preenche campos obrigatórios usando o comando de suporte", () => {
        const customer = {
          firstName: "João",
          lastName: "Silva",
          email: "joao.silva@exemplo.com"
        };

        cy.preencheCamposObrigatorios(customer);

        cy.get("button[type='submit']")
          .as("submitButton")
          .should("not.be.disabled");

        cy.get("#agree").uncheck();

        cy.get("@submitButton").should("be.disabled");

    });

    it("Preenche formulário e confimar os tickets, validando mensagem de sucesso", () => {
        const firstName = "Leonardo";
        const lastName = "Siqueira";
        const fullName = `${firstName} ${lastName}`;

        cy.get("#first-name").type(firstName);
        cy.get("#last-name").type(lastName);
        cy.get("#email").type('leonardogabriel1203@gmail.com');
        cy.get("#ticket-quantity").select("2");
        cy.get("#vip").check();
        cy.get("#social-media").check();
        cy.get("#friend").check();
        cy.get("#requests").type('Lager beer');
        cy.get(".agreement p").should("contain", `I, ${fullName}, wish to buy 2 VIP tickets.`);
        cy.get("#agree").click();
        cy.get("#signature").type(fullName);
        cy.get(".active").click()
        cy.get(".success > p").should("contain", "Ticket(s) successfully ordered.");
    })



});